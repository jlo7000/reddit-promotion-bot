Reddit Promotion Bot
====================

This is a very simple Python script, all it does is reads unread messages and responds to each message with a key.
This script was originally created for Subsoap to promote Faerie Solitaire on Reddit.

Dataset Schema - keys.yaml
--------------------------

You need to create a keys.yaml file, with the following content:

   claimed: {}
   unclaimed: []

Populated Example
~~~~~~~~~~~~~~~~~

   claimed: {kurtschwarz: ABCD-342-EZ}
   unclaimed: [ABCD-342-00, ABCD-342-01, ABCD-342-02, ABCD-342-03]
