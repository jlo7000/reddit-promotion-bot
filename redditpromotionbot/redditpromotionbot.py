import os
import sys
import time
import yaml
import reddit
import logging
from datetime import datetime, timedelta

__author__ = 'Kurt Schwarz <kurt.schwarz@gmail.com>'
__version__ = '1.1.0'


class PromotionBot(object):
    def __init__(self, uname, passwd, keys_dataset):
        self.api = reddit.Reddit(user_agent='Promotion Bot (Python 2.7.2)')
        self.api.login(uname, passwd)
        self.keys_dataset = keys_dataset

    def _validate_redditor(self, redditor):
        if (str(redditor) in self.keys_dataset['claimed-triple'] or
            (datetime.now() - datetime.fromtimestamp(redditor.created)).\
            days < 2):
            return False
        return True

    def run(self):
        while len(self.keys_dataset['unclaimed']):
            messages = []
            for message in self.api.user.get_unread(limit=None):
                messages.append(message)
            logging.debug('Got %i messages.' % len(messages))

            for message in reversed(messages):
                redditor = message.author
                if not self._validate_redditor(redditor):
                    message.mark_as_read()
                    continue

                unclaimed_keys = []
                for x in xrange(0, 3):
                    unclaimed_keys.append(self.keys_dataset['unclaimed'].\
                                          pop().rstrip('\n'))
                logging.debug('Giving %s the keys %s!' % (str(redditor),
                                                          ', '.\
                                                          join(unclaimed_keys)))

                message_ok = False
                try:
                    message.reply('  \n'.join(unclaimed_keys))
                    message.mark_as_read()
                    message_ok = True
                except:
                    logging.exception(('Caught exception will sending the'
                                       ' keys %s to %s!') % (', '.\
                                                             join(unclaimed_keys),
                                                             str(redditor)))

                if not message_ok:
                    for unclaimed_key in unclaimed_keys:
                        self.keys_dataset['unclaimed'].append(unclaimed_key)
                    continue

                self.keys_dataset['claimed-triple'][str(redditor)] =\
                unclaimed_keys
                logging.debug('Great success! %i keys left!' %\
                              len(self.keys_dataset['unclaimed']))

                # Sleep for 2 seconds so you don't upset the reddit gods!
                time.sleep(2)
            time.sleep(4)


def _init_logging():
    logger = logging.getLogger('')
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('')

    file_handle = logging.FileHandler('bot.log')
    file_handle.setFormatter(formatter)
    logger.addHandler(file_handle)

    console_handle = logging.StreamHandler()
    console_handle.setFormatter(formatter)
    logger.addHandler(console_handle)


def main():
    _init_logging()
    bot = PromotionBot('username', 'password', yaml.load(open('keys.yaml',
                                                              'rb')))
    try:
        bot.run()
    except:
        raise
    finally:
        yaml.dump(bot.keys_dataset, open('keys.yaml', 'wb'))

    return 0

if __name__ == '__main__':
    sys.exit(main())
